﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CrawlerService.services
{
    public class CrawlerService
    {
        public static MetaCard getHtmlMeta(string url)
        {
            MetaCard card = new MetaCard();
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc = web.Load(url);
            card.title = doc.DocumentNode.SelectSingleNode("//title").InnerText;
            foreach (HtmlNode metaNode in doc.DocumentNode.SelectNodes("//meta[@property]"))
            {
                HtmlAttribute attribute = metaNode.Attributes["property"];
                if(attribute.Value == "og:image")
                {
                    card.image = metaNode.Attributes["content"].Value;
                } else if (attribute.Value == "og:description")
                {
                    card.description = metaNode.Attributes["content"].Value;
                } else if (attribute.Value == "og:site_name")
                {
                    card.siteName = metaNode.Attributes["content"].Value;
                }
            }

            return card;
        }
    }
}
