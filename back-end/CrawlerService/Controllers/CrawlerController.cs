﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrawlerService.services;

namespace CrawlerService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CrawlerController : ControllerBase
    {

        [HttpGet("crawl-url")]
        public MetaCard Get(string url)
        {
            MetaCard card = CrawlerService.services.CrawlerService.getHtmlMeta(url);
            return new MetaCard
            {
                title = card.title,
                siteName = card.siteName,
                description = card.description,
                image = card.image,
            };
        }
    }
}
