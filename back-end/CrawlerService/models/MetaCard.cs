using System;

namespace CrawlerService
{
    public class MetaCard
    {
        public string title { get; set; }

        public string siteName { get; set; }

        public string description { get; set; }

        public string image { get; set; }
    }
}
